package misyac.yuri.androidproject.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;

public class Levels {
    @SerializedName("Levels")
    private Collection<Level> mLevel = new ArrayList<>();

    public Collection<Level> getLevel() {
        return mLevel;
    }

    public void setLevel(Collection<Level> level) {
        mLevel = level;
    }

    public static class Level {
        @SerializedName("Level")
        private Integer mLevel;
        @SerializedName("Score")
        private Integer mScore;
        @SerializedName("OnClick")
        private Integer mOnClick;

        public Integer getLevel() {
            return mLevel;
        }

        public void setLevel(Integer level) {
            this.mLevel = level;
        }

        public Integer getScore() {
            return mScore;
        }

        public void setScore(Integer score) {
            mScore = score;
        }

        public Integer getOnClick() {
            return mOnClick;
        }

        public void setOnClick(Integer onClick) {
            mOnClick = onClick;
        }
    }
}

package misyac.yuri.androidproject.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CarePlan {

    @SerializedName("Data")
    private CarePlanData mData;

    public CarePlanData getData() {
        return mData;
    }

    public static class CarePlanData {

        @SerializedName("CareplanId")
        private Long mCareplanId;
        @SerializedName("Items")
        private List<CarePlanJson> mList = new ArrayList<>();

        public List<CarePlanJson> getList() {
            return mList;
        }

        public static class CarePlanJson {

            @SerializedName("ItemId")
            private Double mItemId;
            @SerializedName("ItemGuid")
            private String mItemGuid;
            @SerializedName("ImageUrl")
            private String mImageUrl;
            @SerializedName("Category")
            private String mCategory;
            @SerializedName("CategoryId")
            private Integer mCategoryId;
            @SerializedName("ItemType")
            private Integer mItemType;
            @SerializedName("ReadOnly")
            private Boolean mReadOnly;
            @SerializedName("Name")
            private String mName;
            @SerializedName("CarePlanSymptomTypes")
            private List<Integer> mCarePlanSymptomTypes = new ArrayList<>();
            @SerializedName("Days")
            private List<Integer> mDays = new ArrayList<>();
            @SerializedName("DayTimes")
            private List<Integer> mDayTimes = new ArrayList<>();
            @SerializedName("SpecificTimes")
            private List<Integer> mSpecificTimes = new ArrayList<>();
            @SerializedName("UpdatedByUser")
            private String mUpdatedByUser;
            @SerializedName("StartDate")
            private String mStartDate;
            @SerializedName("EndDate")
            private String mEndDate;
            @SerializedName("Value1")
            private Double mValue1;
            @SerializedName("Value2")
            private Double mValue2;
            @SerializedName("Value3")
            private Double mValue3;
            @SerializedName("Value4")
            private Double mValue4;
            @SerializedName("ValueTypeId")
            private Integer mValueTypeId;
            @SerializedName("Description")
            private String mDescription;
            @SerializedName("DrugId")
            private Long mDragId;
            @SerializedName("DrugInternalId")
            private Long mDrugInternalId;
            @SerializedName("DoseTitle")
            private String mDoseTitle;
            @SerializedName("DoseValue")
            private Double mDoseValue;
            @SerializedName("DoseName")
            private String mDoseName;
            @SerializedName("MedicationInternalId")
            private String mMedicationInternalId;
            @SerializedName("HasReminders")
            private Boolean mHasReminders;
            @SerializedName("ComplianceRuleValue")
            private Integer mComplianceRuleValue;
            @SerializedName("NeedRegenerateSchedule")
            private Boolean mNeedRegenerateSchedule;

        }
    }
}

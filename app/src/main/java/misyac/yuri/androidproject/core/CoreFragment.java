package misyac.yuri.androidproject.core;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class CoreFragment extends Fragment{

    @LayoutRes
    private int mLayout;

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(mLayout, container, false);
        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }

    protected void setLayout(@LayoutRes int layout) {
        this.mLayout = layout;
    }

    public abstract void initView();
}

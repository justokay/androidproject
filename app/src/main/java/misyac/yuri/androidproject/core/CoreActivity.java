package misyac.yuri.androidproject.core;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import misyac.yuri.androidproject.R;

public abstract class CoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected Fragment addFragmentTo(@IdRes int res, Class<? extends CoreFragment> clazz, String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            try {
                fragment = clazz.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(clazz.getSimpleName() + " must extent CoreFragment");
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(res, fragment, tag)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(res, fragment, tag)
                    .commit();
        }

        return fragment;
    }

    protected Fragment addFragmentTo(@IdRes int res, CoreFragment fragment, String tag) {
        Fragment fragment_ = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment_ == null) {
            try {
                fragment_ = fragment;
            } catch (Exception e) {
                throw new RuntimeException(fragment.getClass().getSimpleName() + " must extent CoreFragment");
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(res, fragment_, tag)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(res, fragment_, tag)
                    .commit();
        }

        return fragment;
    }
}

package misyac.yuri.androidproject.core;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;

import net.danlew.android.joda.JodaTimeAndroid;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
        mailTo = "yuri.misyac@gmail.com"
)
public class CoreApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        FlowManager.init(this);
        JodaTimeAndroid.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        Log.d("DESTROY", "app");
    }
}

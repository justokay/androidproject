package misyac.yuri.androidproject.core;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceHelper {

    private static final String MAIN_APP_DATA_FILE = "app.data";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(MAIN_APP_DATA_FILE, Context.MODE_PRIVATE);
    }
}

package misyac.yuri.androidproject.camera;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.provider.SyncStateContract;
import android.support.v4.app.Fragment;

import java.io.File;

public class Gallery {

    Activity mActivity;

    public Gallery(Activity _activity){
        this.mActivity = _activity;
    }

    public ComponentName isCanGetGalleryPicture() {
        Intent galleryIntent = IntentUtils.getGalleryStartIntent();
        return galleryIntent.resolveActivity(mActivity.getPackageManager());
    }

    public void openGallery(Fragment _fragment) {
        if (isCanGetGalleryPicture() != null) {
            Intent galleryIntent = IntentUtils.getGalleryStartIntent();
            if (galleryIntent.resolveActivity(mActivity.getPackageManager()) != null) {
//                _fragment.startActivityForResult(galleryIntent, SyncStateContract.Constants.REQUEST_GALLERY_IMAGE);
                _fragment.startActivityForResult(galleryIntent, 1234);
            }
        }
    }

    public void addPhotoToGallery(String _mPhotoFilePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(_mPhotoFilePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        mActivity.sendBroadcast(mediaScanIntent);
    }

}
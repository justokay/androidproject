package misyac.yuri.androidproject.webapi;

public class WebConstant {

    public static final String BASE_URL = "https://ncorp.lifeguardhealthnetworks.com";

    public static final String TEST = "/api/careplan/getcareplanmetadata";

    private WebConstant() {

    }
}

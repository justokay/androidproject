package misyac.yuri.androidproject.webapi;

import misyac.yuri.androidproject.model.response.CarePlan;
import misyac.yuri.androidproject.model.response.Levels;
import retrofit.http.GET;
import rx.Observable;

public interface TestApi {

    @GET("justokay/AndroidProject/master/Levels.json")
    Observable<Levels> getLevels();

    @GET(WebConstant.TEST)
    Observable<CarePlan> getCarePlan();
}

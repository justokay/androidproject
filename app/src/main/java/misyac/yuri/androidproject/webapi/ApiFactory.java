package misyac.yuri.androidproject.webapi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.internal.Util;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import misyac.yuri.androidproject.util.Utils;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class ApiFactory {

    private static final int CONNECT_TIMEOUT = 15;
    private static final int WRITE_TIMEOUT = 60;
    private static final int TIMEOUT = 60;

    private ApiFactory() {

    }

    private static final OkHttpClient CLIENT = new OkHttpClient();

    static {
        CLIENT.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        CLIENT.setWriteTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        CLIENT.setReadTimeout(TIMEOUT, TimeUnit.SECONDS);
    }

    @NonNull
    private static Retrofit getRetrofit(Context context) {

        File httpCacheDirectory = new File(context.getCacheDir(), "responses");

        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);

        CLIENT.setCache(cache);
        CLIENT.interceptors().add(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .addHeader("Authorization", Utils.getHeader())
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });

        return new Retrofit.Builder()
                .baseUrl(WebConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(CLIENT)
                .build();
    }

    @NonNull
    public static TestApi getTestApi(Context context) {
        return getRetrofit(context).create(TestApi.class);
    }
}

package misyac.yuri.androidproject.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import misyac.yuri.androidproject.R;
import misyac.yuri.androidproject.core.CoreFragment;
import misyac.yuri.androidproject.ui.activity.FragmentActivity;

public class TestFragment extends CoreFragment {

    public static final String TAG = TestFragment.class.getSimpleName();
    private static final String KEY_I = "I";

    @Bind(R.id.next)
    Button mNext;
    @Bind(R.id.test)
    TextView mTest;

    private FragmentActivity mActivity;
    private int mInt;

    public static TestFragment newInstance(int i) {
        Bundle args = new Bundle();
        args.putInt(KEY_I, i);
        TestFragment fragment = new TestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentActivity) {
            mActivity = (FragmentActivity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.fragment_test);

        mInt = getArguments().getInt(KEY_I);

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            mActivity.getToolBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void initView() {
        mTest.setText(String.valueOf(++mInt));

        mNext.setOnClickListener(v -> {
            mActivity.addFragmentToBackStack(TestFragment.newInstance(mInt));
        });
    }
}
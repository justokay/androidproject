package misyac.yuri.androidproject.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;

import java.text.DateFormat;
import java.util.Locale;

import butterknife.Bind;
import misyac.yuri.androidproject.R;
import misyac.yuri.androidproject.core.CoreFragment;

public class ListFragment extends CoreFragment {

    public static final String TAG = ListFragment.class.getSimpleName();

    @Bind(R.id.textView)
    TextView mTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLayout(R.layout.fragment_list);
    }

    @Override
    public void initView() {
        int minutes = 127;
        DateTimeFormat.mediumDate().withLocale(Locale.getDefault()).print(minutes);
    }
}
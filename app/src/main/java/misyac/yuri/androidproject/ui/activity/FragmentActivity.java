package misyac.yuri.androidproject.ui.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import misyac.yuri.androidproject.R;
import misyac.yuri.androidproject.core.CoreActivity;
import misyac.yuri.androidproject.ui.fragment.TestFragment;

public class FragmentActivity extends CoreActivity {

    public static final String TAG = FragmentActivity.class.getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.container)
    RelativeLayout mContainer;
    @Bind(R.id.coordinator)
    CoordinatorLayout mCoordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Main");
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        addFragmentTo(R.id.container, TestFragment.newInstance(0), TestFragment.TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                click();
                break;
        }
        return true;
    }

    private void click() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public ActionBar getToolBar() {
        return getSupportActionBar();
    }

    public void addFragmentToBackStack(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .add(R.id.container, fragment)
                .commit();
    }
}
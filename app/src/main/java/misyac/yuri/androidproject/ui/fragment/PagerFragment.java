package misyac.yuri.androidproject.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import misyac.yuri.androidproject.R;
import misyac.yuri.androidproject.core.CoreFragment;

public class PagerFragment extends CoreFragment {

    private static final String ARG_DATE = "date";
    private static final String TAG = PagerFragment.class.getSimpleName();

    @Bind(R.id.date_label)
    protected TextView mDateLabel;
    @Bind(R.id.progress)
    protected ProgressBar mProgressBar;

    private DateTime mDateTime;

    public static PagerFragment newInstance(DateTime dateTime) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, dateTime);
        PagerFragment fragment = new PagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLayout(R.layout.fragment_page_test);
        mDateTime = (DateTime) getArguments().getSerializable(ARG_DATE);
    }

    @Override
    public void initView() {
        DateTimeFormatter formatter = DateTimeFormat.mediumDate().withLocale(Locale.getDefault());
        mDateLabel.setText(formatter.print(mDateTime));
    }

    public void refresh() {
        mProgressBar.setVisibility(View.VISIBLE);
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> {
                    mProgressBar.setVisibility(View.GONE);
                });
            }
        }).start();
    }

}

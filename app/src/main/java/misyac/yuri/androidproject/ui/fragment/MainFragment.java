package misyac.yuri.androidproject.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

import butterknife.Bind;
import butterknife.ButterKnife;
import misyac.yuri.androidproject.R;
import misyac.yuri.androidproject.core.CoreFragment;
import misyac.yuri.androidproject.model.response.CarePlan;
import misyac.yuri.androidproject.webapi.ApiFactory;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class MainFragment extends CoreFragment implements View.OnClickListener {

    public static final String TAG = MainFragment.class.getSimpleName();

    @Bind(R.id.click)
    Button mRequestButton;
    @Bind(R.id.text)
    TextView mText;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.fragment_main);
    }

    @Override
    public void initView() {
        mRequestButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.click:
                testRequest();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        compositeSubscription.unsubscribe();
    }

    private void testRequest() {
        DateTime start = DateTime.now();

        compositeSubscription.add(
                ApiFactory.getTestApi(getActivity()).getCarePlan()
                        .observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<CarePlan>() {
                            @Override
                            public void onCompleted() {
                                Log.e(TAG, "onCompleted: " + Seconds.secondsBetween(start, DateTime.now()) + " sec");
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError:", e);
                            }

                            @Override
                            public void onNext(CarePlan carePlan) {
                                int size = carePlan.getData().getList().size();
                                Log.e(TAG, "onNext: " + size);
                                getActivity().runOnUiThread(() -> {
                                    mText.setText(String.valueOf(size));
                                });
                            }
                        })
        );
    }
}

package misyac.yuri.androidproject.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import misyac.yuri.androidproject.R;
import misyac.yuri.androidproject.core.CoreActivity;
import misyac.yuri.androidproject.ui.fragment.MainFragment;

public class MainActivity extends CoreActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}

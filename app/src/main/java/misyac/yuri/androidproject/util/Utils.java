package misyac.yuri.androidproject.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

//import org.apache.commons.lang3.text.WordUtils;

public class Utils {

    public static final String TAG = Utils.class.getSimpleName();

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    private Utils() {

    }

    public static int generateViewId() {
        for (;;) {
            final int result = sNextGeneratedId.get();
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1;
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static int convertPixelsToDp(Context context, float px){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return (int) dp;
    }

    public static String intToOpacityString(double opacityPercent) {
        opacityPercent = Math.round(opacityPercent * 100) / 100.0d;
        int alpha = (int) Math.round(opacityPercent * 255);
        String hex = Integer.toHexString(alpha).toUpperCase();
        if (hex.length() == 1) hex = "0" + hex;
        return String.format("%s", hex);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    public static float round(float f, int afterPion) {
        BigDecimal bd = new BigDecimal(f);
        bd = bd.setScale(afterPion, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static void clearImageView(ImageView imageView) {
        if (!(imageView.getDrawable() instanceof BitmapDrawable)){
            return;
        }

        BitmapDrawable bitmap = (BitmapDrawable) imageView.getDrawable();
        bitmap.getBitmap().recycle();
        imageView.setImageDrawable(null);
    }

    public static String getAuthHeaderValue(String userName, String password) {
        // Concatenate username and password with a colon for authentication
        final String credentials = userName + ":" + password;
        try {
            return "Basic " + Base64.encodeToString(credentials.getBytes("UTF-8"), Base64.NO_WRAP);
        } catch (UnsupportedEncodingException e) {
            // Watch for encodings carefully and this will not happen
            return "";
        }
    }

    public static String getHeader() {
        return getAuthHeaderValue("alexander.v@ualeaders.com", "qwerty-3");
    }
}
